<?php

set_time_limit(0);


//$files = getAllSongsDirectory($argv[1]);
$files = getAllSongsDirectory('./old');

$counterTotal = count($files);
$counterIndex = 1;
foreach ($files as $fileName) {
	generateLrcFile($fileName);
	printSomething('contador: ' . $counterIndex++ . '/' . $counterTotal . ' (' .$fileName.')');
}


function getAllSongsDirectory($path){
	$files = array();
	foreach (glob("$path/*.lrc") as $file) {
		$file = end(explode('/', $file));
		$files[] = $file;
	}
	return $files;
}



function generateLrcFile($fileName){
	$oldFile = file("./old/" . $fileName );
	$newFile = fopen("./new/" . $fileName, "w");
	$j=0;
	$linesSize = count($oldFile);
	foreach ($oldFile as $num_line => $line) {
		$exportLine = '';
		$lineInitTime = getInitLineTime($line);
		$line = explode("]", $line);
	// $lineInitTime = explode("[", $line[0])[1];
		if(isTimeLine($lineInitTime)){
			$lineTags = '';
			$lineTags = $lineTags . $line[1];
			for($i=2; $i<count($line);$i++){
				$lineTags = $lineTags . ']' . $line[$i];
			}
			$lineTags = explode('<', $lineTags);
			if(count($lineTags)==1){
				$lineMaxTime = '59:59.99';
				if(count($oldFile) > $num_line +1){
					$lineMaxTime = getInitLineTime($oldFile[$num_line + 1]);
				}       
				$lineEndTime = calculateEndTime($lineInitTime,$lineMaxTime,0);

				$exportLine = $lineInitTime . '-' . $lineEndTime;
				$exportLine = $exportLine . '-' . rtrim($lineTags[0]);
				$exportLine = $exportLine . '\\n' . "\r\n";
			}else{
				for ($i=1; $i < count($lineTags); $i++) { 
					$lineMaxTime = '59:59.99';
					if($i+1 < count($lineTags)){
						$lineMaxTime = explode('>', $lineTags[$i+1])[0];
					} else {
						$lineMaxTime = getInitLineTime($oldFile[$num_line + 1]);
					}

					$auxLine = explode('>', $lineTags[$i]);
					$lineInitTime = $auxLine[0];
					$lineEndTime = calculateEndTime($lineInitTime,$lineMaxTime,0);

					
					$exportLine = $exportLine . $lineInitTime;
					$exportLine = $exportLine . '-' . $lineEndTime;
					if($i+1 < count($lineTags)){
						$exportLine = $exportLine . '-' . $auxLine[1];
						$exportLine = $exportLine . "\r\n";
					} else{
						$exportLine = $exportLine . '-' . rtrim($auxLine[1]);
					}
				}
				$exportLine = $exportLine . '\\n';
				$exportLine = $exportLine . "\r\n";
			}
		}
		fwrite($newFile, $exportLine);
	}
	fclose($newFile);
	exec("dos2unix ./new/" . $fileName . ' 2>&1', $outputExec);
}


function isTimeLine($lineInitTime){
	$regex = '/[0-9]{2}:[0-9]{2}.[0-9]{2}/';
	if(preg_match($regex, $lineInitTime)){
		return true;
	}else{
		return false;
	}
}

function sumOneCentesimToTime($time){
	$minute = getMinute($time);
	$second = getSecond($time);
	$centesim = getCentesim($time);
	$centesim = $centesim + 1;
	if($centesim == 100){
		$centesim = '00';
		$second = $second + 1;
		if($second == 60){
			$second = '00';
			$minute = $minute + 1;
		}
	}
	if($minute < 10){
		$minute = '0' . $minute;
	}
	if($second < 10){
		$second = '0' . $second;
	}
	if($centesim < 10 && $centesim != '00'){
		$centesim = '0' . $centesim;
	}
	$time = $minute . ':' . $second . '.' . $centesim;
	return $time;
}

function firstBeforeSecondTime($firstTime, $secondTime){
	if(getMinute($firstTime)<getMinute($secondTime)){
		return true;
	} elseif (getMinute($firstTime)>getMinute($secondTime)) {
		return false;
	}
	if(getSecond($firstTime)<getSecond($secondTime)){
		return true;
	} elseif (getSecond($firstTime)>getSecond($secondTime)) {
		return false;
	}
	if(getCentesim($firstTime)<getCentesim($secondTime)){
		return true;
	} elseif (getCentesim($firstTime)>getCentesim($secondTime)) {
		return false;
	}
	return false;
}

function getMinute($time){
	return intval(explode(':',$time)[0]);
}

function getSecond($time){
	return intval(explode('.', explode(':',$time)[1])[0]);
}

function getCentesim($time){
	return intval(explode('.',$time)[1]);
}

function getInitLineTime($line){
	return explode('[', explode(']', $line)[0])[1];
}

function calculateEndTime($time, $nextTime, $maxIncrease){
	if($maxIncrease == 0){
		$maxIncrease = 50;
	}
	$auxTime = sumOneCentesimToTime($time);
	$endTime = $auxTime;
	$i=1; //ya hemos sumado una vez
	while($i < $maxIncrease){ 
		$auxTime = sumOneCentesimToTime($auxTime);
		if(firstBeforeSecondTime($auxTime, $nextTime)){
			$endTime = $auxTime;
		}else{
			$i = $maxIncrease;
		}
		$i++;
	}
	return $endTime;
}






function printSomething($thing){
	$inConsole = true;

	if(!$inConsole){
		echo '<hr style="border-top: 2px solid green;">';
	}

	if(is_array($thing)){
		var_dump($thing);
	}else{
		echo $thing;
	}

	if(!$inConsole){
		echo '<hr style="border-top: 2px solid red;">';
	}else{
		echo "\r\n";
	}

}